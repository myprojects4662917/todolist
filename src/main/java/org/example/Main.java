package org.example;
import java.util.Scanner;

public class Main {
    public static String[] fillToDoList(int size, Scanner scanner) {
        String[] toDoList = new String[size];

        scanner.nextLine();

        for (int i = 0; i < toDoList.length; i++) {
            System.out.println("Введите дело " + (i + 1));
            toDoList[i] = scanner.nextLine();
        }
        return toDoList;
    }

    public static void printToDoList(String[] toDoList) {
        for (int i = 0; i < toDoList.length; i++) {
            System.out.println((i + 1) + ". " + toDoList[i]);
        }
    }

    public static void completeToDoList(int number, String[] toDoList) {
        if (number <= toDoList.length && number >= 1) {
            toDoList[number - 1] = "Сделано: " + toDoList[number - 1];
        } else {
            System.err.println("Вы ввели неправильный номер!");
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите размер списка дел!");
        int size = scanner.nextInt();

        String[] toDoList = fillToDoList(size, scanner);

        boolean isRunning = true;

        while (isRunning) {
            System.out.println("Ваш список дел: ");
            printToDoList(toDoList);

            System.out.println("Введите номер дела, которое хотите выполнить!");
            int number = scanner.nextInt();
            if (number == 99) {
                isRunning = false;
            } else {
                completeToDoList(number, toDoList);
            }
        }
    }
}